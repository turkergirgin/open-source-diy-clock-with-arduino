/*************************************************************************************************************************
 Open Source DIY Clock with Arduino

 This code, which is shared in open source, belongs to a clock project made with Arduino Nano.  The circuit diagrams and 
 all parts used are included in the project files. The codes and the whole project are licensed using the 
 GNU General Public License v3.0. The features provided by the software and hardware for the clock are as follows.

 - The time, date, and information entered in the settings screen will not be lost even if the power is turned off.
 - The LCD Display light turns off optionally after the set time.
 - The light is switched on automatically after a distance using the distance sensor.
 - Using the temperature and humidity sensor, this information is displayed on the clock display.
 - A circuit and a corresponding setting screen are used to prevent automatic shutdown when the power bank is used.
 - Time and Date information can be updated from the menu.
 - Alarm settings and on-off menu can be updated.
 - Alarm snooze is available.
 - All functional features are designed to be changed from the settings screen.

  created 2019
  by Türker Girgin <turkergirgin@gmail.com>

  This project is licensed under the GNU General Public License v3.0.
  All project files can be found at:
   https://gitlab.com/turkergirgin/open-source-diy-clock-with-arduino
 ************************************************************************************************************************/

#include <EEPROM.h>         //EEPROM kütüphanesi gerekli ayarları ve değişkenleri kalıcı kayıt yapabilmek için yükleniyor
#include <SimpleTimer.h>    //SimpleTimer kütüphanesi zamanlanmış görevleri oluşturmak için yükleniyor
#include <LCD5110_Basic.h>  //Nokia 5110 LCD ekranı kontrol etmek için gerekli kütüphane yükleniyor
#include <virtuabotixRTC.h> //DS1302 Gerçek zamanlı saat modülü için gerekli kütüphane yükleniyor
#include <DHT.h>            //DHT11 Nem ve Sıcaklık sensörü kontrol kütüphanesi yükleniyor

//Nokia 5110 LCD 5110 pin bağlantıları tanımlanıyor
#define LCD_RST 3
#define LCD_CE 4
#define LCD_DC 5
#define LCD_DIN 6
#define LCD_CLK 7
#define LCD_BL 8

//Nokia 5110 LCD için hazırlanan font ve image dosyaları tanımlanıyor
extern uint8_t first_screen[];
extern uint8_t fontsample_full_5110_turko[];
extern uint8_t fontsample_ext_numeric_5110_24_cool_v2[];
extern uint8_t fontsample_ext_numeric_5110_16_v2[];
extern uint8_t thermometer_16_v2[];
extern uint8_t moisture_16_v2[];
extern uint8_t alarm[];
extern uint8_t snooze[];

//RTC1302 Saat modülü için pin bağlantıları tanımlanıyor
#define RTC_CLK_PIN 9
#define RTC_DAT_PIN 10
#define RTC_RST_PIN 11

//DHT11 Nem ve Sıcaklık modülü için pin ve model tanımlaması yapılıyor
#define DHT_PIN 12
#define DHT_TYPE DHT11

//HC-SR04 Ultrasonik mesafe sensörü pin bağlantıları tanımlanıyor
#define HC_TRIG_PIN A1
#define HC_ECHO_PIN A2

//PowerBank Auto Off Disable devresiiçin tetikleme pin bağlantısı tanımlanıyor
#define PB_AOD_PIN A3

//Buzzer için pin tanımlaması yapılıyor
#define BUZZER_PIN 13

//Zamanlanmış görevler için gerekli kesmeler oluşturuluyor
SimpleTimer HC_TIMER;

//Kütüphanelere bağlı sensörler ve LCD panel için gerekli yapı oluşturuluyor
LCD5110 LCD_SCREEN(LCD_CLK, LCD_DIN, LCD_DC, LCD_RST, LCD_CE);
virtuabotixRTC RTC_SAAT(RTC_CLK_PIN, RTC_DAT_PIN, RTC_RST_PIN);
DHT DHT_SENSOR(DHT_PIN, DHT_TYPE);

//Proje içinde kullanılacak değişkenler oluşturuluyor
int Pressed_Key = 0;
int Reading_Key_Value = 0;
int LCD_Back_Light_Time, Set_Back_Light_Time, Snooze = 0;
int Menu_ID = 0;
int Set_Current = 1;
int Set_Key_Tone = 1;
int Set_Lang = 0;
int Set_Contrast = 70;
int Set_Hours, Set_Minutes, Set_DayOfWeek, Set_DayOfMonth, Set_Month, Set_Year;
int Set_Hours_Alarm, Set_Minutes_Alarm, Set_Alarm;
int Set_Day1, Set_Day2, Set_Day3, Set_Day4, Set_Day5, Set_Day6, Set_Day7;
int Set_Distance, Set_PB_Trigger;
bool Set_Save_Time, Set_Save_Date, Set_Save_Settings = false;
bool Alarm_Ring = false;
long HC_Time;
long HC_Distance;
String LCD_Date, LCD_Time, LCD_Temp, LCD_Moisture = "";

//Mesafe sensörü kontrolü için oluşturulan timer olayında çalışacak fonksiyon
void HC_Sensor_Control()
{
  digitalWrite(HC_TRIG_PIN, LOW);       //Mesafe sensörü pasif hale getirildi
  delayMicroseconds(5);                 //5 mikro saniye boyunca işlemciyi durdur
  digitalWrite(HC_TRIG_PIN, HIGH);      //Sensör ses dalgası üretiyor
  delayMicroseconds(10);                //10 mikro saniye boyunca işlemciyi durdur
  digitalWrite(HC_TRIG_PIN, LOW);       //Ses dalgalarının karışmaması için mesafe sensörü pasif hale getirildi
  HC_Time = pulseIn(HC_ECHO_PIN, HIGH); //Ses dalgaları geri dönerken geçen süre hesaplanıyor
  HC_Distance = HC_Time / 29.1 / 2;     //Ölçülen süre mesafeye çevriliyor
  if (HC_Distance < Set_Distance)       //Mesafe ayarlarda belirlenen santimden kısa ise
  {
    if (Set_Back_Light_Time > 0)
      digitalWrite(LCD_BL, HIGH); //LCD arka ışığı açılıyor
    LCD_Back_Light_Time = 0;      //Işık gecikme süresi sıfırlanıyor
  }
  else
  {
    if (LCD_Back_Light_Time < Set_Back_Light_Time * 3) //LCD arka ışığı gecikme süresi kontrol ediliyor
    {
      LCD_Back_Light_Time++; //LCD arka ışığı gecikme süresini 1 arttır
    }
    else //Değilse LCD arka ışığını kapat
    {
      digitalWrite(LCD_BL, LOW);
    }
  }
}

void setup()
{
  //LCD Ekran için dijital çıkış seçiliyor ve aktif hale getiriliyor
  pinMode(LCD_BL, OUTPUT);
  if (Set_Back_Light_Time > 0)
    digitalWrite(LCD_BL, HIGH);
  //LCD ekran başlatılıyor
  LCD_SCREEN.InitLCD();
  //Isı ve Nem sensörü başlatılıyor
  DHT_SENSOR.begin();
  //Mesafe sensörü için portlar ayarlanıyor
  pinMode(HC_TRIG_PIN, OUTPUT);
  pinMode(HC_ECHO_PIN, INPUT);
  //PowerBank Auto Off Teikleme için port ayarlanıyor
  pinMode(PB_AOD_PIN, OUTPUT);
  digitalWrite(PB_AOD_PIN, LOW);
  //Sensörlere ait timer yapılarının süre ayarları ve fonksiyon bağlantıları yapılıyor
  //333 milisaniye değeri ile saniyede yaklaşık 3 kez  mesafe kontrolü yapılmış oluyor
  HC_TIMER.setInterval(333, HC_Sensor_Control);
  //EEPROM da kayıtlı bulunan veriler okunarak ilgili değişkenlere atanıyor
  Set_Hours_Alarm = EEPROM.read(0); // EEPROMun 0 adresindeki veri okunan veri değişkenine aktarılıyor
  if (Set_Hours_Alarm == 255)
    Set_Hours_Alarm = 0;
  delay(10);
  Set_Minutes_Alarm = EEPROM.read(1);
  if (Set_Minutes_Alarm == 255)
    Set_Minutes_Alarm = 0;
  delay(10);
  Set_Day1 = EEPROM.read(2);
  if (Set_Day1 == 255)
    Set_Day1 = 0;
  delay(10);
  Set_Day2 = EEPROM.read(3);
  if (Set_Day2 == 255)
    Set_Day2 = 0;
  delay(10);
  Set_Day3 = EEPROM.read(4);
  if (Set_Day3 == 255)
    Set_Day3 = 0;
  delay(10);
  Set_Day4 = EEPROM.read(5);
  if (Set_Day4 == 255)
    Set_Day4 = 0;
  delay(10);
  Set_Day5 = EEPROM.read(6);
  if (Set_Day5 == 255)
    Set_Day5 = 0;
  delay(10);
  Set_Day6 = EEPROM.read(7);
  if (Set_Day6 == 255)
    Set_Day6 = 0;
  delay(10);
  Set_Day7 = EEPROM.read(8);
  if (Set_Day7 == 255)
    Set_Day7 = 0;
  delay(10);
  Set_Alarm = EEPROM.read(9);
  if (Set_Alarm == 255)
    Set_Alarm = 0;
  delay(10);
  Set_Contrast = EEPROM.read(10);
  if (Set_Contrast == 255)
    Set_Contrast = 70;
  delay(10);
  LCD_SCREEN.setContrast(Set_Contrast);
  Set_Key_Tone = EEPROM.read(11);
  if (Set_Key_Tone == 255)
    Set_Key_Tone = 1;
  delay(10);
  Set_Back_Light_Time = EEPROM.read(12);
  if (Set_Back_Light_Time == 255)
    Set_Back_Light_Time = 10;
  if (Set_Back_Light_Time > 0)
    digitalWrite(LCD_BL, HIGH);
  delay(10);
  Set_Distance = EEPROM.read(13);
  if (Set_Distance == 255)
    Set_Distance = 30;
  delay(10);
  Set_PB_Trigger = EEPROM.read(14);
  if (Set_PB_Trigger == 255)
    Set_PB_Trigger = 1;
  delay(10);
  Set_Lang = EEPROM.read(15);
  if (Set_Lang == 255)
    Set_Lang = 0;
  delay(10);
  //İlk olarak proje tanıtım yazısı ekranda 3 saniye gösteriliyor
  LCD_SCREEN.drawBitmap(0, 0, first_screen, 84, 48);
  delay(3000);
  LCD_SCREEN.clrScr();
  //Serial.begin(9600);
}

void loop()
{
  //Mesafe sensörüne ait timer eğer gerekli ayar ON(65) bırakılmadıysa çalıştırılıyor.
  if (Set_Back_Light_Time > 0 or Set_Back_Light_Time < 65)
    HC_TIMER.run();

  //Saat modülünden bilgiler alınıyor
  RTC_SAAT.updateTime();

  //Hangi tuşa basıldığını belirleyecek fonksiyon, Kontrol edilen
  //değerler aslında Anolog 0 girişinden okunan direnç değerleri
  // Key1~930 / Key2~700 / Key3~515 / NoKey~0
  Reading_Key_Value = analogRead(A0);
  //Eğer 1 numaralı tuşa baıldıysa alınacak aksiyonlar burada tanımlanıyor
  if (Reading_Key_Value > 880 and Reading_Key_Value < 980)
  {
    Pressed_Key = 1;
    LCD_Back_Light_Time = 0;
    if (Set_Back_Light_Time > 0)
      digitalWrite(LCD_BL, HIGH);
    if (Set_Key_Tone == 1)
      tone(BUZZER_PIN, 523, 50);
    LCD_SCREEN.clrScr();
    if (Alarm_Ring == true)
    {
      Alarm_Ring = false;
      noTone(BUZZER_PIN);
      LCD_SCREEN.invert(false);
      Snooze = 0;
      Menu_ID = 44;
    }
    //Menu tuşu ile hangi menünün aktif olduğu belirleniyor
    Menu_ID++;
    if (Menu_ID > 5)
      Menu_ID = 0;
    //Saat Ayarları menüsü aktifse ve henüz değişiklik yapılmadıysa saat değerleri ilgili değişkenlere atanıyor
    if (Menu_ID == 1 and Set_Save_Time == false)
    {
      Set_Hours = RTC_SAAT.hours;
      Set_Minutes = RTC_SAAT.minutes;
    }
    //Tarih Ayarları menüsü aktifse ve henüz değişiklik yapılmadıysa tarih değerleri ilgili değişkenlere atanıyor
    if (Menu_ID == 2 and Set_Save_Date == false)
    {
      Set_DayOfWeek = RTC_SAAT.dayofweek;
      Set_DayOfMonth = RTC_SAAT.dayofmonth;
      Set_Month = RTC_SAAT.month;
      Set_Year = RTC_SAAT.year;
    }
    //Saat Ayarları menüsü değerlerinde değişiklik yapıldıysa güncelleme modülde de yapılıyor
    if (Set_Save_Time == true)
    {
      RTC_SAAT.updateTime();
      RTC_SAAT.setDS1302Time(0, int(Set_Minutes), int(Set_Hours), RTC_SAAT.dayofweek, RTC_SAAT.dayofmonth, RTC_SAAT.month, RTC_SAAT.year);
      Set_Save_Time = false;
      Set_Current = 1;
      Menu_ID = 0;
    }
    //Tarih Ayarları menüsü değerlerinde değişiklik yapıldıysa güncelleme modülde de yapılıyor
    if (Set_Save_Date == true)
    {
      RTC_SAAT.updateTime();
      RTC_SAAT.setDS1302Time(RTC_SAAT.seconds, RTC_SAAT.minutes, RTC_SAAT.hours, Set_DayOfWeek, Set_DayOfMonth, Set_Month, Set_Year);
      Set_Save_Date = false;
      Set_Current = 1;
      Menu_ID = 0;
    }
    //Alarm Ayarları değiştirildiyse EEPROM daki bilgiler de güncelleniyor
    if (Set_Save_Settings == true)
    {
      EEPROM.update(0, Set_Hours_Alarm);
      delay(10);
      EEPROM.update(1, Set_Minutes_Alarm);
      delay(10);
      EEPROM.update(2, Set_Day1);
      delay(10);
      EEPROM.update(3, Set_Day2);
      delay(10);
      EEPROM.update(4, Set_Day3);
      delay(10);
      EEPROM.update(5, Set_Day4);
      delay(10);
      EEPROM.update(6, Set_Day5);
      delay(10);
      EEPROM.update(7, Set_Day6);
      delay(10);
      EEPROM.update(8, Set_Day7);
      delay(10);
      EEPROM.update(9, Set_Alarm);
      delay(10);
      EEPROM.update(10, Set_Contrast);
      delay(10);
      EEPROM.update(11, Set_Key_Tone);
      delay(10);
      EEPROM.update(12, Set_Back_Light_Time);
      delay(10);
      EEPROM.update(13, Set_Distance);
      delay(10);
      EEPROM.update(14, Set_PB_Trigger);
      delay(10);
      EEPROM.update(15, Set_Lang);
      delay(10);
      Set_Save_Settings = false;
      Set_Current = 1;
      Snooze = 0;
      Menu_ID = 0;
    }
    //Tuşa basıldıktan sonra 200 milisaniye beklenerek gereksiz tuş tekrarı engelleniyor
    delay(200);
  }
  //2 numaralı tuşa basıldığında alınacak aksiyonlar
  else if (Reading_Key_Value > 650 and Reading_Key_Value < 750)
  {
    Pressed_Key = 2;
    if (Alarm_Ring == true)
    {
      Alarm_Ring = false;
      noTone(BUZZER_PIN);
      LCD_SCREEN.invert(false);
      Snooze = Snooze + 5;
      if (Snooze > 30)
        Snooze = 0;
      //Menu_ID = 44;
    }
    LCD_Back_Light_Time = 0;
    if (Set_Back_Light_Time > 0)
      digitalWrite(LCD_BL, HIGH);
    if (Set_Key_Tone == 1)
      tone(BUZZER_PIN, 659, 50);
    //Tuşa basıldıktan sonra 200 milisaniye beklenerek gereksiz tuş tekrarı engelleniyor
    delay(200);
  }
  //3 numaralı tuşa basıldığında alıncak aksiyonlar
  else if (Reading_Key_Value > 465 and Reading_Key_Value < 565)
  {
    Pressed_Key = 3;
    if (Alarm_Ring == true)
    {
      Alarm_Ring = false;
      noTone(BUZZER_PIN);
      LCD_SCREEN.invert(false);
      Snooze++;
      if (Snooze > 30)
        Snooze = 0;
      //Menu_ID = 44;
    }
    LCD_Back_Light_Time = 0;
    if (Set_Back_Light_Time > 0)
      digitalWrite(LCD_BL, HIGH);
    if (Set_Key_Tone == 1)
      tone(BUZZER_PIN, 784, 50);
    //Tuşa basıldıktan sonra 200 milisaniye beklenerek gereksiz tuş tekrarı engelleniyor
    delay(200);
  }
  else
  {
    Pressed_Key = 0;
  }

  //PowerBank Auto Off için gerekli kısım
  if (Set_PB_Trigger == 1)
  {
    if (RTC_SAAT.seconds % 30 == 0)   //Saniyenin modu alınarak 30 saniyede bir A3 pininden transistör
      digitalWrite(PB_AOD_PIN, HIGH); //aracılığıyla 300mA yük bindirerek Auto Off devre dışı ediliyor
    else
      digitalWrite(PB_AOD_PIN, LOW);
  }
  else
    digitalWrite(PB_AOD_PIN, LOW);

  //Alarm saati ve günü kontrol ediliyor
  if (Set_Minutes_Alarm + Snooze < 59)
  {
    if (Set_Alarm == 1 and Set_Hours_Alarm == RTC_SAAT.hours and Set_Minutes_Alarm + Snooze == RTC_SAAT.minutes and RTC_SAAT.seconds < 1)
    {
      if (RTC_SAAT.dayofweek == 1)
        if (Set_Day7 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 2)
        if (Set_Day1 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 3)
        if (Set_Day2 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 4)
        if (Set_Day3 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 5)
        if (Set_Day4 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 6)
        if (Set_Day5 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 7)
        if (Set_Day6 == 1)
          Alarm_Ring = true;
    }
  }
  else
  {
    if (Set_Alarm == 1 and Set_Hours_Alarm + 1 == RTC_SAAT.hours and (Set_Minutes_Alarm + Snooze) - 60 == RTC_SAAT.minutes and RTC_SAAT.seconds < 1)
    {
      if (RTC_SAAT.dayofweek == 1)
        if (Set_Day7 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 2)
        if (Set_Day1 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 3)
        if (Set_Day2 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 4)
        if (Set_Day3 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 5)
        if (Set_Day4 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 6)
        if (Set_Day5 == 1)
          Alarm_Ring = true;
      if (RTC_SAAT.dayofweek == 7)
        if (Set_Day6 == 1)
          Alarm_Ring = true;
    }
  }

  //Kontrol sonunda Alarm verilmesi gerekiyorsa işlem burada başlatılıyor.
  if (Alarm_Ring == true)
  {
    Menu_ID = 0;
    if (Set_Back_Light_Time > 0)
      digitalWrite(LCD_BL, HIGH); //LCD arka ışığı açılıyor
    LCD_Back_Light_Time = 0;      //Işık gecikme süresi sıfırlanıyor
    if (RTC_SAAT.seconds % 3 == 0)
      tone(BUZZER_PIN, 50);
    else
      noTone(BUZZER_PIN);
    if (RTC_SAAT.seconds % 2 == 0)
      LCD_SCREEN.invert(true);
    else
      LCD_SCREEN.invert(false);
    //Alarm kulanıcı tarafından kapatılmazsa belirlenen süre sonunda otomatik kapatılıyor
    if (Set_Minutes_Alarm + Snooze + 10 < 59)
    {
      if (Set_Hours_Alarm == RTC_SAAT.hours and Set_Minutes_Alarm + Snooze + 10 == RTC_SAAT.minutes) //Buraya erteleme değişkeni toplama olarak eklenmeli
      {
        Alarm_Ring = false;
        noTone(BUZZER_PIN);
        LCD_SCREEN.invert(false);
        Snooze = 0;
      }
    }
    else
    {
      if (Set_Hours_Alarm + 1 == RTC_SAAT.hours and (Set_Minutes_Alarm + Snooze + 10) - 60 == RTC_SAAT.minutes) //Buraya erteleme değişkeni toplama olarak eklenmeli
      {
        Alarm_Ring = false;
        noTone(BUZZER_PIN);
        LCD_SCREEN.invert(false);
        Snooze = 0;
      }
    }
  }
  //Ayar ekranlarında ışığın kapanması engelleniyor
  if (Menu_ID != 0)
    LCD_Back_Light_Time = 0; //Işık gecikme süresi sıfırlanıyor

  //Menu_ID değişkeni 0 iken saatin gösterildiği ana menü görüntülenir
  if (Menu_ID == 0)
  {
    //LCD_Date değişkeni boşaltılıyor
    LCD_Date = "";
    //LCD Font değiştiriliyor
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    //LCD_Date değişkeni doldurulurken If ile yapılan kontroller gelen veriler tek haneli ise
    //standart olarak iki haneye çevirmek için kullanılıyor
    if (RTC_SAAT.dayofmonth < 10)
      LCD_Date = LCD_Date + "0";
    LCD_Date = LCD_Date + String(RTC_SAAT.dayofmonth) + ".";
    if (RTC_SAAT.month < 10)
      LCD_Date = LCD_Date + "0";
    LCD_Date = LCD_Date + String(RTC_SAAT.month) + ".";
    //Yıl bilgisinin son iki rakamı gösteriliyor
    LCD_Date = LCD_Date + String(RTC_SAAT.year).substring(2, 4) + " ";
    //Bu kısımdaki switch yapısı haftanın gününe göre günün ismini belirliyor
    //Buradaki sistem pazar haftanın ilk günü olacak şekilde kuruldu
    //Sebebi: Sunday, Monday, Tuesday, Wednesday, diye giden sıralama
    switch (RTC_SAAT.dayofweek)
    {
    case 1:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Sun. ";
      else
        LCD_Date = LCD_Date + "Pazar";
      break;
    }
    case 2:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Mon. ";
      else
        LCD_Date = LCD_Date + "Pzrts";
      break;
    }
    case 3:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Tues.";
      else
        LCD_Date = LCD_Date + "Sal{ ";
      break;
    }
    case 4:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Wed. ";
      else
        LCD_Date = LCD_Date + "}r|mb";
      break;
    }
    case 5:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Thu. ";
      else
        LCD_Date = LCD_Date + "Pr|mb";
      break;
    }
    case 6:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Fri. ";
      else
        LCD_Date = LCD_Date + "Cuma ";
      break;
    }
    case 7:
    {
      if (Set_Lang == 0)
        LCD_Date = LCD_Date + "Sat. ";
      else
        LCD_Date = LCD_Date + "Cmrts";
      break;
    }
    }
    LCD_SCREEN.print(LCD_Date, 0, 0);
    //LCD image dosyası (alarm) basılıyor
    if (Set_Alarm == 1)
      LCD_SCREEN.drawBitmap(0, 8, alarm, 6, 24);
    //Bu kısımda sensörlerden toplanan saat, ısı ve nem bilgileri
    //İkinci satıra yazılıyor. If ile yapılan kontroller gelen veriler tek haneli ise
    //standart olarak iki haneye çevirmek için kullanılıyor
    //LCD_Time değişkeni boşaltılıyor
    LCD_Time = "";
    if (RTC_SAAT.hours < 10)
      LCD_Time = LCD_Time + "0";
    LCD_Time = LCD_Time + String(RTC_SAAT.hours);
    if (RTC_SAAT.seconds % 2 == 0) //Saniyenin modu alınarak tek sayılarda farklı : işareti basılıyor
      LCD_Time = LCD_Time + ".";
    else
      LCD_Time = LCD_Time + "/";
    if (RTC_SAAT.minutes < 10)
      LCD_Time = LCD_Time + ("0");
    LCD_Time = LCD_Time + String(RTC_SAAT.minutes);
    LCD_SCREEN.setFont(fontsample_ext_numeric_5110_24_cool_v2);
    LCD_SCREEN.print(LCD_Time, CENTER, 8);
    //LCD image dosyası (snooze) basılıyor
    if (Snooze > 0)
      LCD_SCREEN.drawBitmap(78, 8, snooze, 6, 24);
    //LCD image dosyası (termometre) basılıyor
    LCD_SCREEN.drawBitmap(0, 32, thermometer_16_v2, 8, 16);
    //LCD_Temp değişkeni boşaltılıyor
    LCD_Temp = "";
    if (int(DHT_SENSOR.readTemperature()) < 10)
      LCD_Temp = LCD_Temp + "0";
    LCD_Temp = LCD_Temp + (int(DHT_SENSOR.readTemperature())) + "./";
    LCD_SCREEN.setFont(fontsample_ext_numeric_5110_16_v2);
    LCD_SCREEN.print(LCD_Temp, 9, 32);
    //LCD image dosyası (nem işareti) basılıyor
    LCD_SCREEN.drawBitmap(53, 32, moisture_16_v2, 11, 16);
    //LCD_Moisture değişkeni boşaltılıyor
    LCD_Moisture = "";
    if (int(DHT_SENSOR.readHumidity()) < 10)
      LCD_Moisture = LCD_Moisture + "0";
    LCD_Moisture = LCD_Moisture + int(DHT_SENSOR.readHumidity());
    LCD_SCREEN.print(LCD_Moisture, 66, 32);
  }

  //Menu_ID değişkeni 1 iken saatin ayarlandığı menu görüntülenir
  else if (Menu_ID == 1)
  {
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("SET TIME"), CENTER, 8);
    else
      LCD_SCREEN.print(String("SAAT AYARI"), CENTER, 8);
    //if ile kontrol edilen Set_Current değişkeni ayarlanacak olan kısımın seçimini belitmek için kullanılıyor
    //Ayarlanacak olan kısım invert edilerek basılıyor. Böylece ayarlar arayüzü anlaşılır hale geliyor
    if (Set_Current == 1)
      LCD_SCREEN.invertText(true);
    if (Set_Hours < 10)
      LCD_SCREEN.print("0" + String(Set_Hours), 27, 24);
    else
      LCD_SCREEN.print(String(Set_Hours), 27, 24);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String(":"), 39, 24);
    if (Set_Current == 2)
      LCD_SCREEN.invertText(true);
    if (Set_Minutes < 10)
      LCD_SCREEN.print("0" + String(Set_Minutes), 45, 24);
    else
      LCD_SCREEN.print(String(Set_Minutes), 45, 24);
    LCD_SCREEN.invertText(false);
    //2.Tuşa basıldığında değiştirilecek olan kısım aktif ediliyor
    if (Pressed_Key == 2)
    {
      Set_Current++;
      if (Set_Current > 2)
        Set_Current = 1;
    }
    //3 Numaralı tuş değiştirilecek olan kısmı bir arttırıyor. Sınır değer aşılırsa başa dönülüyor
    else if (Pressed_Key == 3)
    {
      Set_Save_Time = true;
      if (Set_Current == 1)
      {
        Set_Hours++;
        if (Set_Hours > 23)
          Set_Hours = 0;
      }
      else if (Set_Current == 2)
      {
        Set_Minutes++;
        if (Set_Minutes > 59)
          Set_Minutes = 0;
      }
    }
  }
  //Menu_ID değişkeni 2 iken tarihin ayarlandığı menu görüntülenir
  else if (Menu_ID == 2)
  {
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("SET DATE"), CENTER, 8);
    else
      LCD_SCREEN.print(String("TARiH AYARI"), CENTER, 8);
    if (Set_Current == 1)
      LCD_SCREEN.invertText(true);
    if (Set_DayOfMonth < 10)
      LCD_SCREEN.print("0" + String(Set_DayOfMonth), 12, 24);
    else
      LCD_SCREEN.print(String(Set_DayOfMonth), 12, 24);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("."), 24, 24);
    if (Set_Current == 2)
      LCD_SCREEN.invertText(true);
    if (Set_Month < 10)
      LCD_SCREEN.print("0" + String(Set_Month), 30, 24);
    else
      LCD_SCREEN.print(String(Set_Month), 30, 24);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("."), 42, 24);
    if (Set_Current == 3)
      LCD_SCREEN.invertText(true);
    LCD_SCREEN.print(String(Set_Year), 48, 24);
    LCD_SCREEN.invertText(false);
    //Haftanın gününü ayarlamak için switch yapısıyla günün tam adı yazılıyor
    if (Set_Current == 4)
      LCD_SCREEN.invertText(true);
    switch (Set_DayOfWeek)
    {
    case 1:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Sunday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Pazar"), CENTER, 36);
      break;
    }
    case 2:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Monday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Pazartesi"), CENTER, 36);
      break;
    }
    case 3:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Tuesday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Sal{"), CENTER, 36);
      break;
    }
    case 4:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Wednesday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("}ar|amba"), CENTER, 36);
      break;
    }
    case 5:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Thursday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Per|embe"), CENTER, 36);
      break;
    }
    case 6:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Friday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Cuma"), CENTER, 36);
      break;
    }
    case 7:
    {
      if (Set_Lang == 0)
        LCD_SCREEN.print(String("Saturday"), CENTER, 36);
      else
        LCD_SCREEN.print(String("Cumartesi"), CENTER, 36);
      break;
    }
    }
    //2.Tuşa basıldığında değiştirilecek olan kısım aktif ediliyor
    if (Pressed_Key == 2)
    {
      Set_Current++;
      if (Set_Current > 4)
        Set_Current = 1;
    }
    //3 Numaralı tuş değiştirilecek olan kısmı bir arttırıyor. Sınır değer aşılırsa başa dönülüyor
    else if (Pressed_Key == 3)
    {
      Set_Save_Date = true;
      if (Set_Current == 1)
      {
        Set_DayOfMonth++;
        if (Set_DayOfMonth > 31)
          Set_DayOfMonth = 1;
      }
      else if (Set_Current == 2)
      {
        Set_Month++;
        if (Set_Month > 12)
          Set_Month = 1;
      }
      else if (Set_Current == 3)
      {
        Set_Year++;
        if (Set_Year > 2200)
          Set_Year = 2000;
      }
      else if (Set_Current == 4)
      {
        LCD_SCREEN.clrRow(4);
        Set_DayOfWeek++;
        if (Set_DayOfWeek > 7)
          Set_DayOfWeek = 1;
      }
    }
  }
  //Menu_ID değişkeni 3 iken alarmın ayarlandığı menu görüntülenir
  else if (Menu_ID == 3)
  {
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("SET ALARM"), CENTER, 8);
    else
      LCD_SCREEN.print(String("ALARM AYARI"), CENTER, 8);
    if (Set_Current == 1)
      LCD_SCREEN.invertText(true);
    if (Set_Alarm == 0)
      LCD_SCREEN.print(String("[X]"), 32, 16);
    else
      LCD_SCREEN.print(String("[`]"), 32, 16);
    LCD_SCREEN.invertText(false);
    if (Set_Current == 2)
      LCD_SCREEN.invertText(true);
    if (Set_Hours_Alarm < 10)
      LCD_SCREEN.print("0" + String(Set_Hours_Alarm), 27, 24);
    else
      LCD_SCREEN.print(String(Set_Hours_Alarm), 27, 24);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String(":"), 39, 24);
    if (Set_Current == 3)
      LCD_SCREEN.invertText(true);
    if (Set_Minutes_Alarm < 10)
      LCD_SCREEN.print("0" + String(Set_Minutes_Alarm), 45, 24);
    else
      LCD_SCREEN.print(String(Set_Minutes_Alarm), 45, 24);
    LCD_SCREEN.invertText(false);
    if (Set_Current == 4)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("M"), 3, 32);
    else
      LCD_SCREEN.print(String("P"), 3, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 9, 32);
    if (Set_Current == 5)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("T"), 15, 32);
    else
      LCD_SCREEN.print(String("S"), 15, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 21, 32);
    if (Set_Current == 6)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("W"), 27, 32);
    else
      LCD_SCREEN.print(String("}"), 27, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 33, 32);
    if (Set_Current == 7)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("T"), 39, 32);
    else
      LCD_SCREEN.print(String("P"), 39, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 45, 32);
    if (Set_Current == 8)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("F"), 51, 32);
    else
      LCD_SCREEN.print(String("C"), 51, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 57, 32);
    if (Set_Current == 9)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("S"), 63, 32);
    else
      LCD_SCREEN.print(String("C"), 63, 32);
    LCD_SCREEN.invertText(false);
    LCD_SCREEN.print(String("/"), 69, 32);
    if (Set_Current == 10)
      LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("S"), 75, 32);
    else
      LCD_SCREEN.print(String("P"), 75, 32);
    LCD_SCREEN.invertText(false);
    //Alarmın kurulduğu günleri gösterebilmek için günün altına ^ işareti basılıyor
    if (Set_Day1 == 0)
      LCD_SCREEN.print(String(" "), 3, 40);
    else
      LCD_SCREEN.print(String("^"), 3, 40);
    if (Set_Day2 == 0)
      LCD_SCREEN.print(String(" "), 15, 40);
    else
      LCD_SCREEN.print(String("^"), 15, 40);
    if (Set_Day3 == 0)
      LCD_SCREEN.print(String(" "), 27, 40);
    else
      LCD_SCREEN.print(String("^"), 27, 40);
    if (Set_Day4 == 0)
      LCD_SCREEN.print(String(" "), 39, 40);
    else
      LCD_SCREEN.print(String("^"), 39, 40);
    if (Set_Day5 == 0)
      LCD_SCREEN.print(String(" "), 51, 40);
    else
      LCD_SCREEN.print(String("^"), 51, 40);
    if (Set_Day6 == 0)
      LCD_SCREEN.print(String(" "), 63, 40);
    else
      LCD_SCREEN.print(String("^"), 63, 40);
    if (Set_Day7 == 0)
      LCD_SCREEN.print(String(" "), 75, 40);
    else
      LCD_SCREEN.print(String("^"), 75, 40);
    //2.Tuşa basıldığında değiştirilecek olan kısım aktif ediliyor
    if (Pressed_Key == 2)
    {
      Set_Current++;
      if (Set_Current > 10)
        Set_Current = 1;
    }
    //3 Numaralı tuş değiştirilecek olan kısmı bir arttırıyor. Sınır değer aşılırsa başa dönülüyor
    else if (Pressed_Key == 3)
    {
      if (Set_Current == 1)
      {
        Set_Alarm++;
        if (Set_Alarm > 1)
          Set_Alarm = 0;
      }
      Set_Save_Settings = true;
      if (Set_Current == 2)
      {
        Set_Hours_Alarm++;
        if (Set_Hours_Alarm > 23)
          Set_Hours_Alarm = 0;
      }
      else if (Set_Current == 3)
      {
        Set_Minutes_Alarm++;
        if (Set_Minutes_Alarm > 59)
          Set_Minutes_Alarm = 0;
      }
      else if (Set_Current == 4)
      {
        Set_Day1++;
        if (Set_Day1 > 1)
          Set_Day1 = 0;
      }
      else if (Set_Current == 5)
      {
        Set_Day2++;
        if (Set_Day2 > 1)
          Set_Day2 = 0;
      }
      else if (Set_Current == 6)
      {
        Set_Day3++;
        if (Set_Day3 > 1)
          Set_Day3 = 0;
      }
      else if (Set_Current == 7)
      {
        Set_Day4++;
        if (Set_Day4 > 1)
          Set_Day4 = 0;
      }
      else if (Set_Current == 8)
      {
        Set_Day5++;
        if (Set_Day5 > 1)
          Set_Day5 = 0;
      }
      else if (Set_Current == 9)
      {
        Set_Day6++;
        if (Set_Day6 > 1)
          Set_Day6 = 0;
      }
      else if (Set_Current == 10)
      {
        Set_Day7++;
        if (Set_Day7 > 1)
          Set_Day7 = 0;
      }
    }
  }
  //Menu_ID değişkeni 4 iken temel ayarların yapıldığı menu görüntülenir
  else if (Menu_ID == 4)
  {
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("SETTINGS"), CENTER, 1);
    else
      LCD_SCREEN.print(String("AYARLAR"), CENTER, 1);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("Contrast:"), 0, 8);
    else
      LCD_SCREEN.print(String("Kontrast:"), 0, 8);
    if (Set_Current == 1)
      LCD_SCREEN.invertText(true);
    LCD_SCREEN.print(String(Set_Contrast - 45), RIGHT, 8);
    LCD_SCREEN.invertText(false);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("Key Tone:"), 0, 16);
    else
      LCD_SCREEN.print(String("Tu| Sesi:"), 0, 16);
    if (Set_Current == 2)
      LCD_SCREEN.invertText(true);
    if (Set_Key_Tone == 0)
      LCD_SCREEN.print(String("[X]"), RIGHT, 16);
    else
      LCD_SCREEN.print(String("[`]"), RIGHT, 16);
    LCD_SCREEN.invertText(false);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("Light(sec):"), 0, 24);
    else
      LCD_SCREEN.print(String("A.I|{k(sn):"), 0, 24);
    if (Set_Current == 3)
      LCD_SCREEN.invertText(true);
    if (Set_Back_Light_Time < 65)
      LCD_SCREEN.print(String(Set_Back_Light_Time), RIGHT, 24);
    else if (Set_Lang == 0)
      LCD_SCREEN.print(String("ON"), RIGHT, 24);
    else
      LCD_SCREEN.print(String("A}"), RIGHT, 24);
    LCD_SCREEN.invertText(false);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("Dist.(cm):"), 0, 32);
    else
      LCD_SCREEN.print(String("Mesafe(cm):"), 0, 32);
    if (Set_Current == 4)
      LCD_SCREEN.invertText(true);
    LCD_SCREEN.print(String(Set_Distance), RIGHT, 32);
    LCD_SCREEN.invertText(false);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("PB-Trigger:"), 0, 40);
    else
      LCD_SCREEN.print(String("PB-Tetikle:"), 0, 40);
    if (Set_Current == 5)
      LCD_SCREEN.invertText(true);
    if (Set_PB_Trigger == 0)
      LCD_SCREEN.print(String("[X]"), RIGHT, 40);
    else
      LCD_SCREEN.print(String("[`]"), RIGHT, 40);
    //2.Tuşa basıldığında değiştirilecek olan kısım aktif ediliyor
    if (Pressed_Key == 2)
    {
      Set_Current++;
      if (Set_Current > 5)
        Set_Current = 1;
    }
    //3 Numaralı tuş değiştirilecek olan kısmı bir arttırıyor. Sınır değer aşılırsa başa dönülüyor
    else if (Pressed_Key == 3)
    {
      Set_Save_Settings = true;
      if (Set_Current == 1)
      {
        Set_Contrast++;
        if (Set_Contrast > 95)
          Set_Contrast = 45;
        LCD_SCREEN.clrRow(1, 66, 83);
        LCD_SCREEN.setContrast(Set_Contrast);
      }
      else if (Set_Current == 2)
      {
        Set_Key_Tone++;
        if (Set_Key_Tone > 1)
          Set_Key_Tone = 0;
      }
      else if (Set_Current == 3)
      {
        Set_Back_Light_Time = Set_Back_Light_Time + 5;
        if (Set_Back_Light_Time > 65)
          Set_Back_Light_Time = 0;
        LCD_SCREEN.clrRow(3, 66, 83);
      }
      else if (Set_Current == 4)
      {
        Set_Distance++;
        if (Set_Distance > 250)
          Set_Distance = 5;
        LCD_SCREEN.clrRow(4, 66, 83);
      }
      else if (Set_Current == 5)
      {
        Set_PB_Trigger++;
        if (Set_PB_Trigger > 1)
          Set_PB_Trigger = 0;
      }
    }
  }
  //Menu_ID değişkeni 5 iken dil ayarlarının yapıldığı menu görüntülenir
  else if (Menu_ID == 5)
  {
    LCD_SCREEN.setFont(fontsample_full_5110_turko);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("SET LANGUAGE"), CENTER, 8);
    else
      LCD_SCREEN.print(String("DiL AYARI"), CENTER, 8);
    LCD_SCREEN.invertText(true);
    if (Set_Lang == 0)
      LCD_SCREEN.print(String("ENGLISH"), CENTER, 24);
    else
      LCD_SCREEN.print(String("T$RK}E"), CENTER, 24);

    //3 Numaralı tuş değiştirilecek olan kısmı bir arttırıyor. Sınır değer aşılırsa başa dönülüyor
    if (Pressed_Key == 3)
    {
      Set_Save_Settings = true;

      Set_Lang++;
      if (Set_Lang > 1)
        Set_Lang = 0;
      LCD_SCREEN.clrRow(1);
      LCD_SCREEN.clrRow(3);
    }
  }
}
