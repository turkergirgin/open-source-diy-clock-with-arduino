# Open Source DIY Clock with Arduino

###### This project, which is shared in open source, belongs to a clock project made with Arduino Nano. The circuit diagrams and all parts used are included in the project files. The codes and the whole project are licensed using the GNU General Public License v3.0.  
###### The features provided by the software and hardware for the clock are as follows.

- The time, date, and information entered in the settings screen will not be lost even if the power is turned off.
- The LCD Display light turns off optionally after the set time.
- The light is switched on automatically after a distance using the distance sensor.
- Using the temperature and humidity sensor, this information is displayed on the clock display.
- A circuit and a corresponding setting screen are used to prevent automatic shutdown when the power bank is used.
- Time and Date information can be updated from the menu.
- Alarm settings and on-off menu can be updated.
- Alarm snooze is available.
- All functional features are designed to be changed from the settings screen.
  
  
***Breadboard Connections***

![Open Source DIY Clock with Arduino](https://gitlab.com/turkergirgin/open-source-diy-clock-with-arduino/raw/master/Fritzing%20Files/open_source_diy_clock_with_arduino_bb.png)  

![Clock Screen](https://gitlab.com/turkergirgin/open-source-diy-clock-with-arduino/raw/master/LCD%20Screen%20Capture/21.jpg)  

***Component List***

| Amount | Part Type |
| ------ | ------ |
| 1 | Arduino Nano (Rev3.0) (ICSP) |
| 1 | Nokia 5110 LCD | 
| 1 | DHT11 Humitidy and Temperature Sensor (3 pins) |
| 1 | HC-SR04 Ultrasonic Distance Sensor | 
| 1 | RTC_DS1302_module | 
| 1 | Piezo Speaker |
| 1 | NPN-Transistor |
| 1 | Power plug |
| 2 | 1kΩ Resistor |
| 1 | 16Ω Resistor |
| 2 | 10kΩ Resistor |
| 1 | 4.7kΩ Resistor |
| 1 | 220Ω Resistor |
| 3 | Pushbutton |

***Extra Libraries***

- SimpleTimer library for Arduino  
Home Page: https://playground.arduino.cc/Code/SimpleTimer/  
Download: https://github.com/jfturcot/SimpleTimer  
- LCD5110 Basic  
Home Page: http://www.rinkydinkelectronics.com/library.php?id=44  
Download: http://www.rinkydinkelectronics.com/download.php?f=LCD5110_Basic.zip  
- ArduinoRTClibrary  
Home Page: http://playground.arduino.cc/Main/DS1302  
Download: https://github.com/chrisfryer78/ArduinoRTClibrary  
- Arduino library for DHT11, DHT22, etc Temperature & Humidity Sensors  
Version : 1.2.3  
Home Page : https://learn.adafruit.com/dht  
Download : https://github.com/adafruit/DHT-sensor-library  
- EEPROM Library  
Home Page : https://www.arduino.cc/en/Reference/EEPROM  
Download : https://github.com/PaulStoffregen/EEPROM  